-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Jul 21, 2020 at 04:52 PM
-- Server version: 8.0.18
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ads_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE IF NOT EXISTS `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(13, 0, 1, 'Banner', 'fa-image', '/banner', NULL, '2020-07-21 14:32:49', '2020-07-21 14:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `admin_operation_log`
--

DROP TABLE IF EXISTS `admin_operation_log`;
CREATE TABLE IF NOT EXISTS `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 11:47:38', '2020-07-21 11:47:38'),
(2, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 13:46:36', '2020-07-21 13:46:36'),
(3, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 13:46:42', '2020-07-21 13:46:42'),
(4, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"banner\",\"model_name\":\"App\\\\Models\\\\Banner\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\BannerController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"title\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"description\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"image\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"date_from\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"date\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":null,\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":null,\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"VXMlPGZn2zlUdZjupwMT1btF9RAJHr4Mw6TroV3j\"}', '2020-07-21 13:49:02', '2020-07-21 13:49:02'),
(5, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2020-07-21 13:49:02', '2020-07-21 13:49:02'),
(6, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"banner\",\"model_name\":\"App\\\\Models\\\\Banner\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\BannerController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"title\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"description\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"image\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"date_from\",\"type\":\"date\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"date_to\",\"type\":\"date\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"duration\",\"type\":\"integer\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"frequancy\",\"type\":\"integer\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"VXMlPGZn2zlUdZjupwMT1btF9RAJHr4Mw6TroV3j\"}', '2020-07-21 13:51:25', '2020-07-21 13:51:25'),
(7, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2020-07-21 13:51:25', '2020-07-21 13:51:25'),
(8, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"banner\",\"model_name\":\"App\\\\Models\\\\Banner\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\BannerController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"title\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"description\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"image\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"date_from\",\"type\":\"date\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"date_to\",\"type\":\"date\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"duration\",\"type\":\"integer\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"frequancy\",\"type\":\"integer\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"VXMlPGZn2zlUdZjupwMT1btF9RAJHr4Mw6TroV3j\"}', '2020-07-21 13:54:50', '2020-07-21 13:54:50'),
(9, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2020-07-21 13:54:50', '2020-07-21 13:54:50'),
(10, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2020-07-21 14:04:02', '2020-07-21 14:04:02'),
(11, 1, 'admin/banner', 'GET', '127.0.0.1', '[]', '2020-07-21 14:04:07', '2020-07-21 14:04:07'),
(12, 1, 'admin/banner', 'GET', '127.0.0.1', '[]', '2020-07-21 14:20:40', '2020-07-21 14:20:40'),
(13, 1, 'admin/banner/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:20:44', '2020-07-21 14:20:44'),
(14, 1, 'admin/banner/create', 'GET', '127.0.0.1', '[]', '2020-07-21 14:21:54', '2020-07-21 14:21:54'),
(15, 1, 'admin/banner/create', 'GET', '127.0.0.1', '[]', '2020-07-21 14:27:09', '2020-07-21 14:27:09'),
(16, 1, 'admin/banner', 'POST', '127.0.0.1', '{\"title\":\"banner 1\",\"description\":\"banner\",\"date_from\":\"2020-07-21\",\"date_to\":\"2020-07-31\",\"duration\":\"1\",\"Frequency\":\"3\",\"_token\":\"VXMlPGZn2zlUdZjupwMT1btF9RAJHr4Mw6TroV3j\"}', '2020-07-21 14:28:00', '2020-07-21 14:28:00'),
(17, 1, 'admin/banner', 'GET', '127.0.0.1', '[]', '2020-07-21 14:28:01', '2020-07-21 14:28:01'),
(18, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:29:44', '2020-07-21 14:29:44'),
(19, 1, 'admin/auth/menu/1', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"VXMlPGZn2zlUdZjupwMT1btF9RAJHr4Mw6TroV3j\"}', '2020-07-21 14:29:50', '2020-07-21 14:29:50'),
(20, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:29:51', '2020-07-21 14:29:51'),
(21, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:29:57', '2020-07-21 14:29:57'),
(22, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:30:04', '2020-07-21 14:30:04'),
(23, 1, 'admin/auth/menu/2', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"VXMlPGZn2zlUdZjupwMT1btF9RAJHr4Mw6TroV3j\"}', '2020-07-21 14:30:29', '2020-07-21 14:30:29'),
(24, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:30:29', '2020-07-21 14:30:29'),
(25, 1, 'admin/auth/menu/8', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"VXMlPGZn2zlUdZjupwMT1btF9RAJHr4Mw6TroV3j\"}', '2020-07-21 14:30:33', '2020-07-21 14:30:33'),
(26, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:30:33', '2020-07-21 14:30:33'),
(27, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Banner\",\"icon\":\"fa-image\",\"uri\":\"\\/banner\",\"roles\":[null],\"permission\":null,\"_token\":\"VXMlPGZn2zlUdZjupwMT1btF9RAJHr4Mw6TroV3j\"}', '2020-07-21 14:32:49', '2020-07-21 14:32:49'),
(28, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-07-21 14:32:50', '2020-07-21 14:32:50'),
(29, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"VXMlPGZn2zlUdZjupwMT1btF9RAJHr4Mw6TroV3j\",\"_order\":\"[{\\\"id\\\":13}]\"}', '2020-07-21 14:32:52', '2020-07-21 14:32:52'),
(30, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:32:52', '2020-07-21 14:32:52'),
(31, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-07-21 14:32:54', '2020-07-21 14:32:54'),
(32, 1, 'admin/banner', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:32:58', '2020-07-21 14:32:58'),
(33, 1, 'admin/banner', 'GET', '127.0.0.1', '[]', '2020-07-21 14:34:38', '2020-07-21 14:34:38'),
(34, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:34:44', '2020-07-21 14:34:44'),
(35, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 14:34:49', '2020-07-21 14:34:49'),
(36, 1, 'admin/banner', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:34:58', '2020-07-21 14:34:58'),
(37, 1, 'admin/banner', 'GET', '127.0.0.1', '[]', '2020-07-21 14:35:02', '2020-07-21 14:35:02'),
(38, 1, 'admin/banner', 'GET', '127.0.0.1', '[]', '2020-07-21 14:35:09', '2020-07-21 14:35:09'),
(39, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:35:13', '2020-07-21 14:35:13'),
(40, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 14:37:35', '2020-07-21 14:37:35'),
(41, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 14:37:42', '2020-07-21 14:37:42'),
(42, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 14:39:36', '2020-07-21 14:39:36'),
(43, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 14:40:26', '2020-07-21 14:40:26'),
(44, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 14:42:22', '2020-07-21 14:42:22'),
(45, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 14:42:26', '2020-07-21 14:42:26'),
(46, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 14:42:45', '2020-07-21 14:42:45'),
(47, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 14:42:55', '2020-07-21 14:42:55'),
(48, 1, 'admin/banner', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:42:57', '2020-07-21 14:42:57'),
(49, 1, 'admin/banner', 'GET', '127.0.0.1', '[]', '2020-07-21 14:43:48', '2020-07-21 14:43:48'),
(50, 1, 'admin/banner/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:43:52', '2020-07-21 14:43:52'),
(51, 1, 'admin/banner/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:43:54', '2020-07-21 14:43:54'),
(52, 1, 'admin/banner', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:43:56', '2020-07-21 14:43:56'),
(53, 1, 'admin/banner/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:44:00', '2020-07-21 14:44:00'),
(54, 1, 'admin/banner/1/edit', 'GET', '127.0.0.1', '[]', '2020-07-21 14:44:17', '2020-07-21 14:44:17'),
(55, 1, 'admin/banner', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:44:20', '2020-07-21 14:44:20'),
(56, 1, 'admin/banner/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:44:23', '2020-07-21 14:44:23'),
(57, 1, 'admin/banner', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-07-21 14:44:27', '2020-07-21 14:44:27'),
(58, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-07-21 14:44:34', '2020-07-21 14:44:34');

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions`
--

DROP TABLE IF EXISTS `admin_permissions`;
CREATE TABLE IF NOT EXISTS `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`),
  UNIQUE KEY `admin_permissions_slug_unique` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL),
(6, 'Admin helpers', 'ext.helpers', '', '/helpers/*', '2020-07-21 13:45:55', '2020-07-21 13:45:55');

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

DROP TABLE IF EXISTS `admin_roles`;
CREATE TABLE IF NOT EXISTS `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`),
  UNIQUE KEY `admin_roles_slug_unique` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2020-07-21 11:46:39', '2020-07-21 11:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_menu`
--

DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE IF NOT EXISTS `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_permissions`
--

DROP TABLE IF EXISTS `admin_role_permissions`;
CREATE TABLE IF NOT EXISTS `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_permissions`
--

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_users`
--

DROP TABLE IF EXISTS `admin_role_users`;
CREATE TABLE IF NOT EXISTS `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$Rit0C8pTI2o6efxPLpZXFeoCBxrc3WzA6nhRp.gh3Nxa0Q4iJsvZK', 'Administrator', NULL, 'GHkLxjp2ObnpzxsYQTL8FZ2D8b9lZtvIroidBEGyJW57qT6hpZOdd2g7mW5K', '2020-07-21 11:46:39', '2020-07-21 11:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_permissions`
--

DROP TABLE IF EXISTS `admin_user_permissions`;
CREATE TABLE IF NOT EXISTS `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `description`, `image`, `date_from`, `date_to`, `duration`, `frequency`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'banner 1', 'banner', 'images/c6899ae658b5e1d5716c772b1dc754a7.png', '2020-07-21', '2020-07-31', 1, 3, '2020-07-21 14:28:01', '2020-07-21 14:28:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_01_04_173148_create_admin_tables', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_07_21_155450_create_banner_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
