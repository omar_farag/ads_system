<?php

namespace App\Admin\Controllers;

use App\Models\Banner;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class BannerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('Banner List'))
//            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('Banner Details'))
//            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('Edit Banner'))
//            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('Create New Banner'))
//            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Banner);

//        $grid->id('ID');
        $grid->title('Title');
        $grid->description('Description');
        $grid->image('Load Image')->image(url($path = 'uploads') . "/", 100, 100);
        $grid->date_from('Date From');
        $grid->date_to('Date To');
        $grid->duration('Duration');
        $grid->frequency('Frequency');
        $grid->disableExport();
        $grid->disableFilter();


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Banner::findOrFail($id));

        $show->id('ID');
        $show->title('Title');
        $show->description('Description');
        $show->image('Load Image')->image(url($path = 'uploads') . "/", 100, 100);
        $show->date_from('Date From');
        $show->date_to('Date To');
        $show->duration('Duration');
        $show->frequency('Frequency');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Banner);

//        $form->display('ID');
        $form->text('title', 'Title')->rules('required');;
        $form->text('description', 'Description')->rules('required');;
        $form->image('image', 'Load Image')->uniqueName()->rules('required');
        $form->date('date_from', 'Date From')->rules('required');;
        $form->date('date_to', 'Date To')->rules('required');;
        $form->number('duration', 'Duration')->default(1)->min(1)->rules('required');;
        $form->number('Frequency', 'Frequency')->default(1)->min(1)->rules('required');;
//        $form->display(trans('admin.created_at'));
//        $form->display(trans('admin.updated_at'));

        return $form;
    }
}
